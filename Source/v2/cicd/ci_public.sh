#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
set -x
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
repo_root="$(realpath "${source_root}/../../")"
cd "${repo_root}"

DO_NOT_TRACK="true" ; export DO_NOT_TRACK
EARTHLY_DISABLE_ANALYTICS="true" ; export EARTHLY_DISABLE_ANALYTICS

rm -f -r "${source_root}/.cache"
git clean -f -x . || echo " [ERR] Git-clean failed!"

earthly +public-v2

cd ./Build/v2_earthly_BuildDir/artifacts/public/
find . -type f -exec ls -hl {} +
