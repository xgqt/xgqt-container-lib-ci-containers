#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Remove old dangling OCI containers images.

.Parameter OCIExe
Alternative OCI container executor to use.

.Example
./remove_dangling_images.ps1
#>

param(
    [string] $OCIExe = "docker",

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$danglingImages = @(
    & $OCIExe images --filter "dangling=true" --quiet
)

if ($danglingImages) {
    & $OCIExe rmi @danglingImages

    if ($LASTEXITCODE -ne 0) {
        Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

        throw "$OCIExe rmi failed"
    }
}
