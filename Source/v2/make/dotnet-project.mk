# -*- make -*-

include ../make/dotnet-exports.mk
include ../make/tools.mk

DOTNET-CONFIGURATION    := Release
DOTNET-OUTPUT           :=
DOTNET-PROJECT-DIR      :=
DOTNET-PROJECT          :=

.PHONY: all
all:
	$(MAKE) clean
	$(MAKE) publish

.PHONY: clean
clean:
	@$(RM) $(DOTNET-OUTPUT)
	@$(RM) $(DOTNET-PROJECT-DIR)/.cache

.PHONY: publish
publish:
	@$(DOTNET) publish							\
		--configuration $(DOTNET-CONFIGURATION)	\
		--output $(DOTNET-OUTPUT)				\
		--verbosity quiet						\
		$(DOTNET-PROJECT)
