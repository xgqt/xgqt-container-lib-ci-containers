#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$Env:PSModulePath += ":$PSScriptRoot/../../../ci-posh-util/src:"

Import-Module PSOCIUtil
Import-Module SimpleLog

function Invoke-RunOCIImage {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $BuildDirectoryPath,
        [Parameter(Mandatory)] [string] $OCIRegistry,
        [Parameter(Mandatory)] [string] $OCIRunner,
        [Parameter(Mandatory)] [string] $Process
    )

    $fullBuildDirectoryPath = (Get-Item $BuildDirectoryPath).FullName

    Set-Location -Path $fullBuildDirectoryPath

    $repoFilePath = Join-Path -Resolve `
        -Path $fullBuildDirectoryPath -ChildPath "./src/REPO"

    if (-Not (Test-Path $repoFilePath)) {
        throw [System.IO.FileNotFoundException] `
            "REPO file not found for: $fullBuildDirectoryPath"
    }

    $imageRepo = Get-ConfigContent $repoFilePath
    $completeImageRepo = $OCIRegistry + "/" + $imageRepo

    $dockerArgs = @(
        "run",
        "--attach=STDOUT",
        "--interactive",
        "--rm",
        "--tty",
        $completeImageRepo,
        $Process
    )

    $ociExe = (Get-Command $OCIRunner).Path

    Format-SimpleLog -MessageType Debug `
        -MessageContent "Running from subproject: $BuildDirectoryPath"
    Format-SimpleLog -MessageType Success `
        -MessageContent "Running image: $completeImageRepo"

    $dockerProcess = Start-Process -NoNewWindow -PassThru -Wait `
        -FilePath $ociExe -ArgumentList $dockerArgs

    if ($dockerProcess.ExitCode -ne 0) {
        Format-SimpleLog -MessageType Error `
            -MessageContent "Children process exited with error"

        throw "Container run failed."
    }
}

Export-ModuleMember -Function Invoke-RunOCIImage
Export-ModuleMember -Function Invoke-RunOCIImage
