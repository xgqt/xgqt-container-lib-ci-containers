# ChangeLog

## 2.1.0

* admin
    * 328df6a admin: use mkdocs_wrapper.sh
    * e9723da admin/remove_old_images.ps1: support OCIRegistry
    * 6daafd2 CPSTester/CPSTester.psm1: support OCIRegistry
    * 79ce218 cicd/ci_build.sh: set OCI_EXE; limit to certain subprojects
* ci-container-dotnet-tools
    * ffe80ac ci-container-dotnet-tools/src/ARGS.json: remove .NET 7
* ci-container-opensuse-dotnet-tools
    * e21e2c5 v2: drop ci-container-opensuse-dotnet-tools
* Jenkinsfile
    * 32439c9 Jenkinsfile: checkout scm
* powerprepare
    * ea2cf18 dotnet-tools.json: add powerprepare.app
    * 7922685 add PowerPrepare.yaml
* ci-container-ubuntu-cc
    * ca0422c ci-container-debian-cc: install curl and wget
    * 56512a6 ci-container-ubuntu-cc: install curl and wget
* ci-container-dash-micro
    * f0f2c9c ci-container-dash-micro: copy busybox and link some core utilities
    * 7ecc795 add ci-container-dash-micro
* admin
    * 2cca412 redo_image.ps1: use pwsh_wrapper.sh
    * 937def3 admin: add redo_image.ps1
* ci-container-emacs-tools
    * a407d10 ci-container-emacs-tools: upgrade eldev to 1.8.2
* ci-container-debian-cc
    * 51cc407 ci-container-debian-cc/src/TAGGING.json: change tagging schema
    * dd96372 add ci-container-debian-cc
* v2
    * 7e78ffe v2: add mkdocs_wrapper.sh
    * 9f2d25e v2: fail when system command fails in ps1 PowerShell scripts
    * 29846aa v2: correct subproject versions - 1.3.0 -> 2.0.0

## 2.0.0

* bump major version to v2
    * 6b19a41 README.md: update for v2
    * a06f9de v2: v1.sln -> v2.sln
    * fc88ab9 v2/cicd/ci_public.sh: v1 -> v2
    * b1443b4 Jenkinsfile: transition to v2
    * 300603a Earthfile: transition to v2
    * ff56e29 initial transition to v2
* add USE_PWSH_TOOL
    * 37df530 cicd: USE_PWSH_TOOL in ci_build.sh and ci_test.sh
* bump powershell version
    * ccefc19 dotnet-tools.json: bump powershell to 7.4.1
* remove Perl dependency
    * 9f297eb v2: perl cleanups
* add ci-ubuntu-cc
    * 0719824 ci-containers-container-ubuntu-cc: remove unwanted tests
    * b9b237e add ci-containers-container-ubuntu-cc
* bump ci-containers-container-racket-tools to use racket 8.12-full
    * b750997 ci-containers-container-racket-tools: bump racket/racket to 8.12-full
* support nix
    * fad67af nix: add shell_hook.sh and use it in shell.nix
    * 8a9d5c7 shell.nix: add commentary
    * 96c5da7 v1: add shell.nix
* use SimpleLog
    * 868cf17 v1: replace WriteColored with SimpleLog
* bump to alpine 3.19.1
    * 38486ee ci-containers-container-*: bump alpine tag to 3.19.1

## 1.3.0

* bump alpine tag
    * 0aad5db v1: bump alpine image to 3.19.0
* add container testing
    * 73e4d11 CPSTester.psm1: color all output
    * 5851053 CPSTester.psm1: do not make replacements
    * c55b334 CPSTester.psm1: optimize case
    * 7d7418e CPSTester.psm1: add Get-TestSuiteNode function
    * c13d01d CPSTester.psm1: add Get-TestFiles function
    * 4b13f89 CPSTester.psm1: test -> tests
    * b1ee048 Makefile: update all recipe
    * eec52f9 CPSTester.psm1: fix match
    * 5a58b5e Test_Executables.xml: add "test zsh version"
    * 36a5d6a test-exes.xml -> Test_Executables.xml
    * d251b87 CPSTester.psm1: pretty error output
    * 9f8b1d7 test_all_images.ps1: try-catch
    * 1bbafbe add a test runner
* use GiLab CI/CD
    * 724edc0 add gitlab-ci configuration
* new ci-containers-plpspy-container container
    * 70cb1dc add ci-containers-plpspy-container
* add cpsdoc
    * e921863 CPSDoc.psm1: use larger strings
    * 5e22f14 CPSDoc.psm1: show configuration files
    * 2f80e61 add Pipfile and Pipfile.lock
    * 8676d50 admin: add build_doc.ps1
    * 0d91364 add ci-containers-documentation
    * 1850c65 add ci-containers-cpsdoc-ps
* new ci-containers-racket-frog-container image
    * 6ef8e73 ci-containers-racket-frog-container: add DEPEND
    * a2ee7be add ci-containers-racket-frog-container

## 1.2.0

* use PowerShell 7.4.0
    * 4e91491 Update dependency powershell to v7.4.0
* perl tweaks
    * 2fa71fc .editorconfig: set perl indent
    * 5875ffd .vscode/.gitignore: ignore perl-lang dir
* use .NET 8.0
    * f1c6c87 Jenkinsfile: update .NET version to 8.0
    * 90842d4 ci-containers-dotnet-tools-container: bump to .NET 8.0
* jenkins integration fixes
    * b7068c4 Jenkinsfile: cleanup
    * eff73ca Jenkinsfile: oci registry parameter
    * 834ed23 Jenkinsfile: disable login again
    * bcef5a0 Jenkinsfile: attempt to login
    * c7c7996 Jenkinsfile: only build for now
    * 913e315 Jenkinsfile: fix syntax
    * 792e6fd Jenkinsfile: tweaks
    * 8cef8c5 Jenkinsfile: restore tools; check version file
* add login_registry.ps1
    * 1285f3e Makefile: add login recipe
    * e0f2e5e admin: add login_registry.ps1
* use psm1 modules
    * bc78e35 ci-containers-run-image-ps: move to real psm1 modules
    * 819260d ci-containers-regen-tag-ps: move to real psm1 modules
    * b7c5d48 ci-containers-push-image-ps: move to real psm1 modules
    * 977e1d9 ci-containers-build-image-ps: move to real psm1 modules
    * d0d848e remove executable bit from plain modules
* support earthly
    * 49a84d6 add initial Earthfile
* add ci-containers-pwsh-oci-tools
    * 77d740c add ci-containers-pwsh-oci-tools
* suppoer buildah
    * 2feed8e build-image.ps1: support buildah

## 1.1.0

* pyproject integration
    * e52c89f Source/v1: add pyproject.toml
* add 4 container images
    * 52aea8d add ci-containers-gentoo-tools
    * 1394f47 add ci-containers-nodejs-tools
    * 5efb6fe add ci-containers-emacs-tools
    * 4311cc3 add ci-containers-alpine-shell-tools
* add build scripts in PowerShell
    * ab5c35e build-image.ps1: colors
    * 503598a init
