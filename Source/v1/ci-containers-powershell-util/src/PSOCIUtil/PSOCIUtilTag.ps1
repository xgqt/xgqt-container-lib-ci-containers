#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

function Get-CompleteImageTag {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $SemanticVersion,
        [Parameter(Mandatory)] [string] $LocalTag,

        [hashtable] $ArgsJson,
        [array] $TaggingJson
    )

    $tagElements = @(
        $SemanticVersion,
        ".",
        $LocalTag
    )

    if ($ArgsJson -and $TaggingJson) {
        # Add a dash separator.
        $tagElements += "-"

        foreach ($tagRule in $TaggingJson) {
            switch ($tagRule.GetType().Name) {
                "String" {
                    $tagElements += $tagRule

                    break
                }

                "OrderedHashtable" {
                    $variableName = $tagRule.variable

                    if (-not $variableName) {
                        Write-Output "ERROR: $variableName is null"

                        throw "Variable name is not set."
                    }

                    $variableValue = $ArgsJson[$variableName]

                    $replacements = $tagRule.replace

                    if ($replacements) {
                        foreach ($replacement in $replacements.GetEnumerator()) {
                            $variableValue = $variableValue `
                                -replace $replacement.Key, $replacement.Value
                        }
                    }

                    $tagElements += $variableValue
                }

                default {
                    throw "Object type is not supported."
                }
            }
        }
    }

    return ($tagElements -join "")
}
