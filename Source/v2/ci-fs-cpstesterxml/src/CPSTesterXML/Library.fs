// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

namespace CPSTesterXML

open System.Xml.Linq
open System.Management.Automation

[<Cmdlet(VerbsCommon.New, "CPSTesterConfig")>]
type NewCPSTesterConfigCommand() =

    inherit PSCmdlet()

    [<Parameter>]
    [<ValidateNotNullOrEmpty>]
    member val XMLString: string = "" with get, set

    override this.BeginProcessing() =
        let result = new PSObject()
        let properties = result.Properties

        let xmlDocument = XDocument.Parse(this.XMLString)

        let testSuiteXElement = xmlDocument.Root
        let testSuiteTitle = testSuiteXElement.Attribute("title").Value

        let testCases: list<PSObject> =
            testSuiteXElement.Elements("TestCase")
            |> Seq.map (fun element -> (new TestCaseXmlMapper(element)).ToPSObject())
            |> List.ofSeq

        properties.Add(new PSNoteProperty("Title", testSuiteTitle))
        properties.Add(new PSNoteProperty("TestCases", testCases))

        this.WriteObject(result)
