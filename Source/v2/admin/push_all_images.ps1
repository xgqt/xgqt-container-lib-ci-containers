#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Push all OCI containers.

.Parameter OCIExe
Alternative OCI container pusher to use.
Defaults to "docker".

.Parameter OCIRegistry
Alternative OCI registry to use.
Defaults to "docker.io".

.Example
./push_all_images.ps1
#>

param(
    [string] $OCIExe = "docker",
    [string] $OCIRegistry = "docker.io",

    [switch] $Help
)

$ErrorActionPreference = "Stop"

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-posh-push-image/src:"

Import-Module PushAllOCIImages

$sourceRoot = Join-Path -Resolve -Path $PSScriptRoot -ChildPath ..

Invoke-PushAllOCIImages `
    -BaseDirectory $sourceRoot `
    -OCIExe $OCIExe `
    -OCIRegistry $OCIRegistry
