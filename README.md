# CI Containers

My collection of containers mainly used for CI/CD purposes.
All available on [Docker Hub](https://hub.docker.com/u/xgqt/).

## Browsing the collection

All containers are published on
the [Docker Hub](https://hub.docker.com/u/xgqt/).
Additionally, you can browse the auto-generated documentation on the GitLab
pages.
For example you can see the documentation for
[ci-gentoo-tools](https://xgqt.gitlab.io/xgqt-container-lib-ci-containers/containers/CI_Container_ci-gentoo-tools/).

## Documentation

### Browse online

* [GitLab pages](https://xgqt.gitlab.io/xgqt-container-lib-ci-containers/)

### Build locally

Install required software:

* make
* powershell
* python (and mkdocs, mkdocs-material, pygments)

Then execute following command:

``` shell
make -C Source/v2 doc
```

## Usage

### Buildah

To use `ci-pwsh-oci-tools` effectively you will need to run it with fuse
mounted and specific security labels disabled.

```shell
docker run --device /dev/fuse:rw        \
    --security-opt seccomp=unconfined   \
    --security-opt apparmor=unconfined  \
    --rm -it -v ./:/repo:ro             \
    xgqt/ci-pwsh-oci-tools
```

## Development

### Using Powershell

If you do not have Powershell (`pwsh`) installed but have the .NET SDK
(and the `dotnet` command) on your system,
then you can use `dotnet tool restore` to install `pwsh`.
If using Powershell as a local tool run it with `dotnet tool run pwsh`.

## License

### Code

Code in this project is licensed under the MPL, version 2.0.

    This Source Code Form is subject to the terms of the Mozilla Public
    License, v. 2.0. If a copy of the MPL was not distributed with this
    file, You can obtain one at http://mozilla.org/MPL/2.0/.
