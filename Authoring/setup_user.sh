#!/bin/sh

set -e
trap 'exit 128' INT

if [ -z "${2}" ] ; then
    echo " Please provide a e-mail address and a user'a real name (quoted)."

    exit 1
fi

user_email="${1}"
user_name="${2}"

git config --local user.email "${user_email}"
git config --local user.name "${user_name}"
