#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Serve built documentation.

.Description
Serve documentation of ci-containers.

.Outputs
None.
#>

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

$sourceRoot = Join-Path -Resolve -Path $PSScriptRoot -ChildPath ..

Set-Location -Path "${sourceRoot}/ci-containers-documentation/src/"

& "${sourceRoot}/pwsh_wrapper.sh" "$PSScriptRoot/build_doc.ps1"

if ($LASTEXITCODE -ne 0) {
    Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

    throw "build_doc.ps1 failed"
}

& "${sourceRoot}/mkdocs_wrapper.sh" serve $args

if ($LASTEXITCODE -ne 0) {
    Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

    throw "mkdocs serve failed"
}
