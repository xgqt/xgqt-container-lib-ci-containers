#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Format F# source files.

.Description
Automatically pick up and format F# source files from this repository.

.Example
./format_fs.ps1
#>

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

Import-Module SimpleLog

Set-Location "$PSScriptRoot/../"

$includedFileExts = @("*.fs", "*.fsi", "*.fsx")
$filePaths = Get-ChildItem -File -Include $includedFileExts -Path . -Recurse
| Where-Object { $_.ResolvedTarget -notlike "*/.cache/*" }

foreach ($filePath in $filePaths) {
    $scriptName = $filePath.Name

    Format-SimpleLog -MessageType Debug -MessageContent "Formatting file: $scriptName"

    & "$PSScriptRoot/../dotnet_wrapper.sh" tool run `
        fantomas --verbosity detailed $filePath

    if ($LASTEXITCODE -ne 0) {
        Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

        throw "dotnet_wrapper.sh failed"
    }
}
