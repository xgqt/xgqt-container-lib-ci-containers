#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
set -x
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
cd "${source_root}"

: "${OCI_EXE:=podman}"
: "${OCI_REGISTRY:=docker.io}"

echo "${OCI_EXE}"
echo "${OCI_REGISTRY}"

USE_PWSH_TOOL="YES"
export USE_PWSH_TOOL

rm -f -r "${source_root}/.cache"
git clean -f -x . || echo " [ERR] Git-clean failed!"

make OCI-EXE="${OCI_EXE}" OCI-REGISTRY="${OCI_REGISTRY}" clean
make OCI-EXE="${OCI_EXE}" OCI-REGISTRY="${OCI_REGISTRY}" deps

allowed_subprojects="
    ci-container-alpine-python
    ci-container-alpine-shell-tools
    ci-container-cpsdoc-mkdocs
    ci-container-dash-micro
    ci-container-emacs-tools
"
for allowed_subproject in ${allowed_subprojects} ; do
    sh ./pwsh_wrapper.sh                            \
       ./admin/build_image.ps1                      \
       -BuildDirectoryPath "${allowed_subproject}"  \
       -OCIExe "${OCI_EXE}"                         \
       -OCIRegistry "${OCI_REGISTRY}"
done

make OCI-EXE="${OCI_EXE}" OCI-REGISTRY="${OCI_REGISTRY}" clean-images
make OCI-EXE="${OCI_EXE}" OCI-REGISTRY="${OCI_REGISTRY}" -s list \
    | grep 'xgqt/' | nl
