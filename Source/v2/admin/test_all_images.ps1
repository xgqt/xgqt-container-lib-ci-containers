#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Test all OCI containers.

.Parameter OCIExe
Alternative OCI container executor to use.

.Example
./test_all_images.ps1
#>

param(
    [string] $OCIExe = "docker",
    [string] $OCIRegistry = "docker.io",

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-posh-util/src:"

Import-Module PSOCIUtil
Import-Module SimpleLog

$success = $true
$RootDirectory = Get-Item "$PSScriptRoot/../"

Get-ChildItem -Directory -Path $RootDirectory
| Where-Object { $_.Name -like "ci-container-*" }
| Where-Object { Test-HasPathContainerfile $_.ResolvedTarget $_.Name }
| ForEach-Object {
    $name = $_.Name

    Format-SimpleLog -MessageType Debug -MessageContent "Running tests from: $name"

    try {
        & "$PSScriptRoot/../pwsh_wrapper.sh" "$PSScriptRoot/test_image.ps1" `
            -OCIExe $OCIExe -OCIRegistry $OCIRegistry $_.ResolvedTarget

        if ($LASTEXITCODE -ne 0) {
            Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

            throw "test_image.ps1 failed"
        }
    }
    catch {
        Format-SimpleLog -MessageType Error -MessageContent "Tests for $name failed"

        $success = $false
    }
}

if ($success) {
    Format-SimpleLog -MessageType Success -MessageContent "All tests have passed"
}
else {
    Format-SimpleLog -MessageType Error -MessageContent "Some tests have failed"

    throw "Tests failed"
}
