#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Login to a OCI container registry.

.Parameter OCIRegistryUser

.Parameter OCIRegistryPassword

.Parameter OCIRegistry

.Parameter OCIExe
Alternative OCI container pusher to use.
Defaults to "docker".

.Example
./login_registry.ps1 localhost:5000 user password
#>

param(
    [string] $OCIRegistryUser,
    [string] $OCIRegistryPassword,

    [string] $OCIRegistry = "docker.io",
    [string] $OCIExe = "docker",

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$argumentList = @(
    "login",
    "--username", $OCIRegistryUser,
    "--password-stdin",
    $OCIRegistry
)

Write-Output $OCIRegistryPassword | & $OCIExe $argumentList
