#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$Env:PSModulePath += ":$PSScriptRoot/../../../ci-containers-powershell-util/src:"

Import-Module PSOCIUtil
Import-Module SimpleLog

function Invoke-BuildAllOCIImages {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [bool] $AutomaticOptions,
        [Parameter(Mandatory)] [string] $BaseDirectory,
        [Parameter(Mandatory)] [string] $BuilderScript,
        [Parameter(Mandatory)] [string] $OCIRegistry,

        [string] $OCIExe,
        [string] $PSExe = "pwsh"
    )

    Format-SimpleLog -MessageType Debug -MessageContent "Builder script is: $BuilderScript"

    Set-Location -Path $BaseDirectory

    Get-ChildItem -Directory -Path $BaseDirectory
    | Where-Object { $_.Name -like "ci-containers-container-*" }
    | Where-Object { Test-HasPathContainerfile $_.ResolvedTarget $_.Name }
    | ForEach-Object {
        Format-SimpleLog -MessageType Debug `
            -MessageContent "Building from subproject: $($_.Name)"

        $argumentList = @(
            $BuilderScript,
            "-BuildDirectoryPath", $_.ResolvedTarget,
            "-OCIRegistry", $OCIRegistry
        )

        if (-not $AutomaticOptions) {
            $argumentList += "-NoAutomaticOptions"
        }

        if ($OCIExe) {
            $argumentList += "-OCIExe $OCIExe"
        }

        Format-SimpleLog -MessageType Info `
            -MessageContent "Executing builder: $argumentList"

        $ociBuilderProcess = Start-Process -NoNewWindow -PassThru -Wait `
            -FilePath $PSExe -ArgumentList $argumentList

        if ($ociBuilderProcess.ExitCode -ne 0) {
            throw "Children process exited with error."
        }

        Format-SimpleLog -MessageType Success `
            -MessageContent "Subproject $($_.Name) built successfully"
    }
}

Export-ModuleMember -Function Invoke-BuildAllOCIImages
