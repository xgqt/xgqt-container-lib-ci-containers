#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

# This script will try to pick any available "dotnet" command.
# It will first search "dotnet" inside the "DOTNET_ROOT" directory.

set -e
set -u
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "$(realpath "${script_path}")")"

: "${DOTNET_ROOT:=/usr/bin}"
dotnet_exe="${DOTNET_ROOT}/dotnet"

if [ ! -x "${dotnet_exe}" ] ; then
    echo " [WRN] DOTNET_ROOT is set to ${DOTNET_ROOT} but ${DOTNET_ROOT}/dotnet executable does not exist!"
    echo " [DBG] Attempting to install dotnet-sdk for local project."

    dotnet_exe="${script_root}/3rd-party/xo-depot-tools/Source/v1/xo-depot-tools/src/wrap-dotnet-sdk.bash"
fi

exec "${dotnet_exe}" "${@}"
