#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$ErrorActionPreference = "Stop"

$buildDirectory = "$($PSScriptRoot)/../../../../../Build"
$binhostDirectory = "$($buildDirectory)/v1_gentoobinhost_BuildDir/binhost"
$packages = @(
    "app-portage/gentoolkit",
    "dev-libs/tree-sitter",
    "dev-libs/tree-sitter-bash",
    "dev-python/chardet",
    "dev-python/lazy-object-proxy",
    "dev-python/snakeoil",
    "dev-python/tree-sitter",
    "dev-util/pkgcheck",
    "dev-util/pkgdev",
    "dev-vcs/git",
    "sys-apps/config-site",
    "sys-apps/pkgcore",
    "sys-devel/crossdev"
)

if (Test-Path $binhostDirectory) {
    Remove-Item -Force -Path $binhostDirectory -Recurse
}

New-Item -Force -Path $binhostDirectory -Type Directory | Out-Null

& docker run `
    --name binhost-creator-0 `
    --rm `
    --volume "$($binhostDirectory):/var/cache/binpkgs" `
    xgqt/ci-gentoo-tools `
    quickpkg --include-config=y --umask 0000 @packages

if ($LASTEXITCODE -ne 0) {
    Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

    throw "docker run failed"
}
