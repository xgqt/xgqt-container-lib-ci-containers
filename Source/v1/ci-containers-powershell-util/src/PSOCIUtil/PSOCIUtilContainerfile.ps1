#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Check for Containerfile.

.Description
Check if a given path has a Containerfile.

.Parameter DirectoryPath
Directory path to check.

.Parameter Name
Alternative name to use for the given directory.

.Inputs
None.

.Outputs
True if a Containerfile exists, false otherwise.
#>

function Test-HasPathContainerfile {
    [CmdletBinding()]

    [OutputType([bool])]

    param(
        [Parameter(Mandatory)] [string] $DirectoryPath,
        [string] $Name = $DirectoryPath
    )

    $containerfile = Join-Path `
        -Path $DirectoryPath `
        -ChildPath "./src/containers/Containerfile"

    if (-not (Test-Path $containerfile)) {
        Write-Output "WARNING: Containerfile not found for: $Name"

        return $false
    }

    return $true
}
