#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
cd "${source_root}"

stamp_directory="${source_root}/.cache/stamps"
restore_stamp_file="${stamp_directory}/psdeps_installed.stamp"

: "${USE_PWSH_TOOL:=}"

if [ ! -f "${restore_stamp_file}" ] ; then
    set -x

    "${script_root}/restore_tools.sh"

    if [ -n "${USE_PWSH_TOOL}" ] ; then
        "${source_root}/dotnet_wrapper.sh" \
            tool run powerprepare --powershell-tool install
    else
        "${source_root}/dotnet_wrapper.sh" tool run powerprepare install
    fi

    mkdir -p "${stamp_directory}"
    touch "${restore_stamp_file}"
fi
