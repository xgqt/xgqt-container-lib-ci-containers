# -*- make -*-

MAKE            ?= make
SH              ?= sh

CP              := cp
DOTNET          := dotnet
FIND            := find
OCI-EXE         := docker
PERL            := perl
PWSH            := $(PERL) $(ADMIN)/pwsh_wrapper.pl
PWSHSCRIPT      := $(PWSH) -NonInteractive
PYTHON          := python3
RM              := rm -f -r
