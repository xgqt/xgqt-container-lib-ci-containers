#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Remove old OCI containers.

.Parameter OCIExe
Alternative OCI container executor to use.

.Example
./remove_old_images.ps1
#>

param(
    [string] $OCIExe = "docker",
    [string] $OCIRegistry = "docker.io",

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-posh-util/src:"

Import-Module PSOCIUtil
Import-Module SimpleLog

$rootDirectory = Get-Item "$PSScriptRoot/../"
$semanticVersion = Get-ConfigContent "$($rootDirectory)/VERSION"

$suspiciousImages = @(& $OCIExe image ls --format "{{.Repository}}:{{.Tag}}")
| Where-Object { $_ -like "*xgqt/ci-*" }
| Where-Object { $_ -notlike "*:<none>" }
| Sort-Object

$safeImages = Get-ChildItem -Directory -Path $rootDirectory
| Where-Object { $_.Name -like "ci-container-*" }
| Where-Object { Test-HasPathContainerfile $_.ResolvedTarget $_.Name }
| ForEach-Object {
    Set-Location `
        -Path (Join-Path -Path $_.ResolvedTarget -ChildPath "src")

    $localTag = Get-ConfigContent "./TAG"
    $imageRepo = Get-ConfigContent "./REPO"

    $taggingJson = Get-JsonContent "./TAGGING.json"
    $argsJsonMultiple = Get-JsonContent "./ARGS.json"

    $recentTags = @(
        "$($OCIRegistry)/$($imageRepo):$($semanticVersion)"
        "$($OCIRegistry)/$($imageRepo):latest"
    )

    foreach ($argsJsonSingle in $argsJsonMultiple) {
        $completeImageTag = Get-CompleteImageTag `
            -ArgsJson $argsJsonSingle -TaggingJson $taggingJson `
            -SemanticVersion $semanticVersion -LocalTag $localTag

        $recentTags = $recentTags + `
            "$($OCIRegistry)/$($imageRepo):$($completeImageTag)"
    }

    $recentTags
}

foreach ($suspiciousImage in $suspiciousImages) {
    if (($safeImages | Where-Object { $_ -like $suspiciousImage }) -or
        ($safeImages | Where-Object { $_ -like "*/$suspiciousImage" })) {
        Format-SimpleLog -MessageType Debug `
            -MessageContent "Keeping recent image: $($suspiciousImage)"
    }
    else {
        Format-SimpleLog -MessageType Debug `
            -MessageContent "Found old image: $($suspiciousImage)"

        & $OCIExe rmi $suspiciousImage

        if ($LASTEXITCODE -ne 0) {
            Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

            throw "$OCIExe rmi $suspiciousImage failed"
        }

        Format-SimpleLog -MessageType Success `
            -MessageContent "Removed old image: $($suspiciousImage)"
    }
}
