#!/usr/bin/env perl

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

use autodie;
use strict;
use warnings;

use Cwd;
use File::Basename;
use FindBin;

my $current_directory = getcwd();

if (`which mkdocs 2>/dev/null`) {
    exec 'mkdocs', @ARGV;
}
elsif (`which pipenv 2>/dev/null`) {
    chdir( dirname $FindBin::Bin );
    system 'pipenv', 'install', '--dev';

    chdir($current_directory);
    exec 'pipenv', 'run', 'mkdocs', @ARGV;
}
else {
    die 'Could not run mkdocs';
}
