#!/usr/bin/env perl

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

use autodie;
use strict;
use warnings;

my $dotnet_exe;
my $dotnet_root = $ENV{DOTNET_ROOT};

if ($dotnet_root) {
    my $dotnet_root_exe = "${dotnet_root}/dotnet";

    if ( -e $dotnet_root_exe ) {
        $dotnet_exe = $dotnet_root_exe;
    }
}

# If after above we still do not have "$dotnet_exe".
if ( !$dotnet_exe ) {
    if ( not `which dotnet 2>/dev/null` ) {
        die 'The "dotnet" executable was not found';
    }

    $dotnet_exe = 'dotnet';
}

exec $dotnet_exe, @ARGV;
