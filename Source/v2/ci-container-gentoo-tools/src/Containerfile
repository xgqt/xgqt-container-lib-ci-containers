# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

ARG REGISTRY
ARG GENTOO_PORTAGE_IMAGE_REPO
ARG GENTOO_PORTAGE_IMAGE_TAG
ARG GENTOO_STAGE3_IMAGE_REPO
ARG GENTOO_STAGE3_IMAGE_TAG

FROM ${REGISTRY}/${GENTOO_PORTAGE_IMAGE_REPO}:${GENTOO_PORTAGE_IMAGE_TAG} as portage
FROM ${REGISTRY}/${GENTOO_STAGE3_IMAGE_REPO}:${GENTOO_STAGE3_IMAGE_TAG}

LABEL description="Gentoo OCI image for for CI/CD purposes"
LABEL maintainer="xgqt@xgqt.org"
LABEL url="https://gitlab.com/xgqt/xgqt-container-lib-ci-containers/"

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo

RUN rm /etc/portage/make.conf \
    && mkdir -p /etc/portage/make.conf /etc/portage/package.use

COPY ./configuration/0000_make.conf /etc/portage/make.conf/
COPY ./configuration/0000_use_git.conf /etc/portage/package.use/
COPY ./configuration/sets_gentoo-tools /etc/portage/sets/gentoo-tools

RUN echo "en_US.UTF-8 UTF-8" >> /etc/locale.gen \
    && locale-gen \
    && eselect locale set en_US.UTF-8
RUN eselect news read all >/dev/null
RUN getuto
RUN emerge --noreplace @gentoo-tools

RUN rm -f -r \
    /var/cache/binpkgs/* /var/cache/distfiles/* \
    /var/log/portage/* /var/log/emerge-fetch.log /var/log/emerge.log
