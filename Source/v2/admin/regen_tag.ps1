#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Regenerate OCI container tag.

.Description
Regenerate local tag of a OCI container in specified directory.

.Parameter DirectoryPath
Path to a OCI container directory.

.Parameter Help
Show help message and exit.

.Example
./regen_tag.ps1 ./ci-container-dotnet-tools
#>

param(
    [string] $DirectoryPath,

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-posh-regen-tag/src:"

Import-Module RegenTag

Invoke-RegenTag -DirectoryPath $DirectoryPath
