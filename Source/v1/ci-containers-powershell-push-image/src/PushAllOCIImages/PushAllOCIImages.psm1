#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$Env:PSModulePath += ":$PSScriptRoot/../../../ci-containers-powershell-util/src:"

Import-Module PSOCIUtil
Import-Module SimpleLog

function Invoke-PushAllOCIImages {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $BaseDirectory,
        [Parameter(Mandatory)] [string] $OCIExe,
        [Parameter(Mandatory)] [string] $OCIRegistry
    )

    Set-Location -Path $BaseDirectory

    Get-ChildItem -Directory -Path $BaseDirectory
    | Where-Object { $_.Name -like "ci-containers-container-*" }
    | Where-Object { Test-HasPathContainerfile $_.ResolvedTarget $_.Name }
    | ForEach-Object {
        $repoFilePath = Join-Path `
            -Resolve `
            -Path $_.ResolvedTarget `
            -ChildPath "./src/options/REPO"

        $imageRepo = Get-ConfigContent $repoFilePath
        $completeImageRepo = $OCIRegistry + "/" + $imageRepo

        $argumentList = @(
            "push",
            "--all-tags",
            $completeImageRepo
        )

        Format-SimpleLog -MessageType Debug `
            -MessageContent "Pushing from subproject: $($_.Name)"
        Format-SimpleLog -MessageType Success `
            -MessageContent "Pushing image: $completeImageRepo"

        $ociProcess = Start-Process -NoNewWindow -PassThru -Wait `
            -FilePath $OCIExe -ArgumentList $argumentList

        if ($ociProcess.ExitCode -ne 0) {
            throw "Children process exited with error."
        }

        Format-SimpleLog -MessageType Debug -MessageContent "Image $($_.Name) pushed"
    }
}

Export-ModuleMember -Function Invoke-PushAllOCIImages
