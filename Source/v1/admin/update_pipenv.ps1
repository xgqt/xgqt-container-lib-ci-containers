#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Update Pipenv environment.

.Description
Automatically pick up and update the Pipenv environment of this repository.

.Example
./update_pipenv.ps1
#>

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

Set-Location "$PSScriptRoot/../"

& pipenv update --dev

& pipenv requirements | Set-Content -Path requirements.txt
& pipenv requirements --dev-only | Set-Content -Path requirements-dev.txt
