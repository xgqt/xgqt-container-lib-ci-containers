#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
List containers built from this repository.

.Description
List container images built from xgqt-container-lib-ci-containers repository.

.Parameter OCIExe
Alternative OCI container pusher to use.
Defaults to "docker".

.Example
./list_images.ps1 -OCIExe podman
#>

param(
    [string] $OCIExe = "docker",
    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-posh-util/src:"

Import-Module PSOCIUtil

Set-Location "$PSScriptRoot/../"

Get-Item "*/src/REPO"
| ForEach-Object {
    $repo = Get-ConfigContent $_

    & $OCIExe image ls `
        --format "{{.Repository}}:{{.Tag}}" `
        --filter "reference=${repo}*"
}
| Where-Object { $_ -notlike "*:<none>" }
| Sort-Object
