// This Source Code Form is subject to the terms of the Mozilla Public
// License, v. 2.0. If a copy of the MPL was not distributed with this
// file, You can obtain one at http://mozilla.org/MPL/2.0/.

namespace CPSTesterXML

open System.Xml.Linq
open System.Management.Automation

type TestCaseXmlMapper(element: XElement) =

    member val Element: XElement = element with get

    member this.ToPSObject() : PSObject =
        let testCasePSO = new PSObject()
        let properties = testCasePSO.Properties

        let element = this.Element
        let title = element.Attribute("title").Value
        let commandBlock = element.Element("CommandBlock").Value.Trim()
        let expectedResult = element.Element("ExpectedResult").Value.Trim()

        properties.Add(new PSNoteProperty("Title", title))
        properties.Add(new PSNoteProperty("CommandBlock", commandBlock))
        properties.Add(new PSNoteProperty("ExpectedResult", expectedResult))

        testCasePSO
