#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
OCI image runner.

.Description
OCI image run script for the xgqt-container-lib-ci-containers repository.
Run a image that was built from a given source directory.

.Parameter BuildDirectoryPath
Path to a OCI container build directory.

.Parameter Process
Process to run inside container.
Defaults to "sh".

.Parameter OCIRegistry
Alternative OCI registry to use.
Defaults to "docker.io".

.Parameter OCIRunner
Alternative OCI container runner to use.
Defaults to "docker".

.Parameter Help
Show help message and exit.

.Example
./run_image.ps1 ./ci-containers-dotnet-tools

.Example
./run_image.ps1 ./ci-containers-dotnet-tools -Process "dotnet fsi"
#>

param(
    [string] $BuildDirectoryPath,

    [string] $OCIRegistry = "docker.io",
    [string] $OCIRunner = "docker",
    [string] $Process = "sh -l",

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-containers-powershell-run-image/src:"

Import-Module RunOCIImage

Invoke-RunOCIImage `
    -BuildDirectoryPath $BuildDirectoryPath `
    -OCIRegistry $OCIRegistry `
    -OCIRunner $OCIRunner `
    -Process $Process
