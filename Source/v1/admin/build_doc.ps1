#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Build documentation.

.Description
Automatically build documentation from this repository.

.Example
./build_doc.ps1
#>

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

$Env:PSModulePath += ":$PSScriptRoot/../ci-containers-powershell-cpsdoc/src:"

Import-Module CPSDoc

Set-Location -Path "$PSScriptRoot/../"

Get-ChildItem -Directory -Path "."
| Where-Object { $_.Name -like "ci-containers-container-*-*" }
| Where-Object { Test-HasPathContainerfile $_.ResolvedTarget $_.Name }
| ForEach-Object {
    Write-Output " Generating documentation of: $($_.Name)"

    Invoke-CPSDocGenerate `
        -BaseDirectory $_.ResolvedTarget `
        -OutputDirectory "./ci-containers-documentation/src/docs/containers/"
}

Set-Location -Path "./ci-containers-documentation/src/"

& "$PSScriptRoot/mkdocs_wrapper.pl" build $args
