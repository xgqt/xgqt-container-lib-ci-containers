#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

function Invoke-OCIExe {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $ExePath,
        [Parameter(Mandatory)] [array] $Arguments
    )

    $ociProcess = Start-Process -NoNewWindow -PassThru -Wait `
        -FilePath $ExePath -ArgumentList $Arguments

    if ($ociProcess.ExitCode -ne 0) {
        throw "OCI executable exited with error."
    }
}
