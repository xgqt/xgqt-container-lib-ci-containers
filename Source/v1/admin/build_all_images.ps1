#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Build all OCI containers.

.Parameter OCIExe
Alternative OCI container builder to use.

.Parameter OCIRegistry
Alternative OCI registry to use.
Defaults to "docker.io".

.Example
./build_all_images.ps1
#>

param(
    [string] $OCIExe,
    [string] $OCIRegistry = "docker.io",
    [switch] $NoAutomaticOptions = $false,

    [switch] $Help
)

$ErrorActionPreference = "Stop"

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-containers-powershell-build-image/src:"

Import-Module BuildAllOCIImages

$sourceRoot = Join-Path -Resolve -Path $PSScriptRoot -ChildPath ..

Invoke-BuildAllOCIImages `
    -AutomaticOptions (-not $NoAutomaticOptions) `
    -BaseDirectory $sourceRoot `
    -BuilderScript "$PSScriptRoot/build_image.ps1" `
    -OCIExe $OCIExe `
    -OCIRegistry $OCIRegistry `
    -PSExe "$PSScriptRoot/pwsh_wrapper.pl"
