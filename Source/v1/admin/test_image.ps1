#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
OCI image tester.

.Description
OCI image test tool for the xgqt-container-lib-ci-containers repository.

.Parameter BaseDirectory
Path to a OCI container base directory.

.Parameter OCIExe
Alternative OCI container builder to use.
If not set, then "docker" is picked first, "podman" second.

.Parameter Help
Show help message and exit.

.Example
./test_image.ps1 ./ci-containers-dotnet-tools
#>

param(
    [string] $BaseDirectory,
    [string] $OCIExe = "docker",

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-containers-powershell-cpstester/src:"

Import-Module CPSTester

Invoke-CPSTest `
    -BaseDirectory $BaseDirectory `
    -OCIExe $OCIExe
