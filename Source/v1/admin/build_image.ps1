#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
OCI image builder.

.Description
OCI image build tool for the xgqt-container-lib-ci-containers repository.

.Parameter BuildDirectoryPath
Path to a OCI container build directory.

.Parameter OCIExe
Alternative OCI container builder to use.
If not set, then "docker" is picked first, "podman" second.

.Parameter OCIRegistry
Alternative OCI registry to use.
Defaults to "docker.io".

.Parameter NoAutomaticOptions
Disable automatic options that would be passed to the selected OCI container
builder.

.Parameter Help
Show help message and exit.

.Example
./build_image.ps1 ./ci-containers-dotnet-tools
#>

param(
    [string] $BuildDirectoryPath,
    [string] $OCIExe,

    [string] $OCIRegistry = "docker.io",
    [switch] $NoAutomaticOptions = $false,

    [switch] $Help
)

$ErrorActionPreference = "Stop"

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$Env:PSModulePath += ":$PSScriptRoot/../ci-containers-powershell-build-image/src:"

Import-Module BuildOCIImage

Invoke-BuildOCIImage `
    -BuildDirectoryPath $BuildDirectoryPath `
    -OCIExe $OCIExe `
    -OCIRegistry $OCIRegistry `
    -AutomaticOptions (-not $NoAutomaticOptions)
