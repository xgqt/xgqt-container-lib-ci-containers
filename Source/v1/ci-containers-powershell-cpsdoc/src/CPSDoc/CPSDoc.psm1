#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$Env:PSModulePath += ":$PSScriptRoot/../../../ci-containers-powershell-util/src:"

Import-Module PSOCIUtil

function Invoke-CPSDocGenerate {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $BaseDirectory,
        [Parameter(Mandatory)] [string] $OutputDirectory
    )

    $srcURL = "https://gitlab.com/xgqt/xgqt-container-lib-ci-containers/-/tree/master/Source/v1"

    $baseDirectoryName = (Get-Item -Path $BaseDirectory).Name

    $docDirectory = Join-Path -Path $BaseDirectory -ChildPath "doc"
    $srcDirectory = Join-Path -Path $BaseDirectory -ChildPath "src"

    $configurationPath = Join-Path -Path $srcDirectory -ChildPath "configuration"
    $containersPath = Join-Path -Path $srcDirectory -ChildPath "containers"
    $optionsPath = Join-Path -Path $srcDirectory -ChildPath "options"

    $aboutFile = Join-Path -Path $docDirectory -ChildPath "About.md"

    $containerFile = Join-Path -Path $containersPath -ChildPath "Containerfile"

    $argsFile = Join-Path -Path $optionsPath -ChildPath "ARGS.json"
    $repoFile = Join-Path -Path $optionsPath -ChildPath "REPO"

    $repoContent = Get-ConfigContent $repoFile
    $repoShortName = $repoContent -replace ".*/", ""

    $output = ""
    $outputFile = Join-Path `
        -Path $OutputDirectory `
        -ChildPath ("CI_Container_" + $repoShortName + ".md")

    $dockerHubURL = "https://hub.docker.com/r/" + $repoContent

    $output += "# $repoShortName

## Overview

* DockerHub repository: [$repoContent]($dockerHubURL)
* Container source: [$baseDirectoryName]($srcURL/$baseDirectoryName/src/)`n"

    if (Test-Path $aboutFile) {
        $aboutContent = Get-ConfigContent $aboutFile

        $output += "### About

$aboutContent
`n"
    }

    $dockerLatest = "docker.io/" + $repoContent + ":latest"

    $output += "## Usage

### Pull $repoShortName

Pull $repoShortName image from dockerhub with:

``````shell
docker pull $dockerLatest
``````

### Run $repoShortName

Run $repoShortName image with:

``````shell
docker run -it --rm --name $repoShortName $dockerLatest sh -l
``````

## Source
`n"

    if (Test-Path $argsFile) {
        $argsContent = Get-ConfigContent $argsFile

        $output += "### Arguments

Argument combinations passed to $repoShortName build:

``````json
$argsContent
``````
`n"
    }

    $containerContent = Get-ConfigContent $containerFile

    $output += "### Containerfile

Source of the $repoShortName container image Containerfile:

```````dockerfile
$containerContent
``````
`n"

    if (Test-Path $configurationPath) {
        $output += "### Configuration files

Source of configuration files used inside the container:
`n"

        Get-ChildItem -File -Path $configurationPath -Recurse
        | ForEach-Object {
            $configurationFileContent = Get-Content -Raw -Path $_.ResolvedTarget

            $output += "#### $($_.Name)

``````
$configurationFileContent
``````
`n"
        }
    }

    Set-Content -Path $outputFile -Value $output.Trim()
}

Export-ModuleMember -Function Invoke-CPSDocGenerate
