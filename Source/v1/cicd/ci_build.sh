#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
set -x
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

source_root="$(realpath "${script_root}/../")"
cd "${source_root}"

: "${OCI_REGISTRY:=docker.io}"

echo "${OCI_REGISTRY}"

make OCI-REGISTRY="${OCI_REGISTRY}" build
make clean-images

sh cicd/support/ci_support_list_images.sh
