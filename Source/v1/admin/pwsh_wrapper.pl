#!/usr/bin/env perl

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

use autodie;
use strict;
use warnings;

use FindBin;

my @pwsh_args = ();

if (`which pwsh 2>/dev/null`) {
    push( @pwsh_args, 'pwsh' );
}
else {
    system "${FindBin::Bin}/restore_tools.pl";

    push( @pwsh_args,
        "${FindBin::Bin}/dotnet_wrapper.pl",
        'tool', 'run', 'pwsh' );
}

exec @pwsh_args, '-NoLogo', '-NoProfile', @ARGV;
