{ pkgs ? import <nixpkgs> {} }:

pkgs.mkShell {
  nativeBuildInputs = with pkgs.buildPackages; [
    # Native project-specific packages
    gnumake
    podman

    # .NET packages
    dotnet-sdk_8
    fantomas
    powershell

    # Python packages
    python312Packages.mkdocs
    python312Packages.mkdocs-material
    python312Packages.pygments

    # Fundamental packages
    git
    glibcLocales
  ];

  shellHook = ''
    ./nix/shell_hook.sh
  '';
}
