#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

function Get-ConfigContent {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $Path
    )

    $fileContent = Get-Content -Encoding UTF-8 -Raw -Path $Path
    $contentTrimmed = $fileContent.Trim()

    return $contentTrimmed
}

function Get-JsonContent {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $Path
    )

    return (Test-Path $Path) `
        ? (Get-ConfigContent $Path | ConvertFrom-Json -AsHashtable) `
        : $null
}
