#!/usr/bin/env perl

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

use autodie;
use strict;
use warnings;

use File::Basename;
use FindBin;

chdir (dirname (dirname $FindBin::Bin));

system 'make', 'doc';
