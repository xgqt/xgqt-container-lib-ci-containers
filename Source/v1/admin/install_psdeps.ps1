#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Install Powershell dependencies.

.Example
./install_psdeps.ps1
#>

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

$availableModules = (Get-Module -ListAvailable).Name
$wantedModules = @(
    "PSScriptAnalyzer"
    "SimpleLog"
)

$wantedModules | ForEach-Object {
    if ($availableModules.Contains($_)) {
        Write-Output " Module already available: $_"
    }
    else {
        Write-Output " Installing module: $_"

        Install-Module -AcceptLicense -Force -Scope CurrentUser -Name $_
    }
}
