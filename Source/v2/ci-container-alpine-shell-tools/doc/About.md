Primary focus is the use for checking POSIX/Bash shell scrips with `shellcheck`.

Alternatively one can use the official
[shellcheck](https://hub.docker.com/r/koalaman/shellcheck/)
docker image if only the `shellcheck` tool is needed.
