#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$Env:PSModulePath += ":$PSScriptRoot/../../../ci-fs-cpstesterxml/publish:"
$Env:PSModulePath += ":$PSScriptRoot/../../../ci-posh-util/src:"

Import-Module CPSTesterXML
Import-Module PSOCIUtil
Import-Module SimpleLog

function Get-TestFiles {
    <#
    .Synopsis
    Gather the test XML files.

    .Parameter TestDirectory
    Where to gather tests from.

    .Outputs
    Array of test files.
    #>

    [CmdletBinding()]

    [OutputType([object[]])]

    param(
        [Parameter(Mandatory)] [string] $TestDirectory
    )

    if (Test-Path -Path $testDirectory) {
        $items = Get-ChildItem -Filter "*.xml" -Path $testDirectory -Recurse

        return $items.ResolvedTarget
    }

    return @()
}

function Invoke-CPSTest {
    <#
    .Synopsis
    Test a container.

    .Description
    Test a container based on a given directory with it's configuration.

    .Parameter BaseDirectory
    Directory to check of tests and container configuration.

    .Parameter OCIExe
    Alternative OCI container pusher to use.

    .Parameter OCIRegistry
    OCI container registry hostname to use.
    #>

    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $BaseDirectory,
        [Parameter(Mandatory)] [string] $OCIExe,
        [Parameter(Mandatory)] [string] $OCIRegistry
    )

    $passedTests = 0
    $failedTests = 0

    $srcDirectory = Join-Path -Path $BaseDirectory -ChildPath "src"
    $testDirectory = Join-Path -Path $BaseDirectory -ChildPath "test"

    $repoFile = Join-Path -Path $srcDirectory -ChildPath "REPO"

    $repoContent = Get-ConfigContent $repoFile
    $repoImage = "$($OCIRegistry)/$($repoContent):latest"

    $testFiles = Get-TestFiles $testDirectory

    foreach ($testFile in $testFiles) {
        $testFileContent = Get-ConfigContent $testFile
        $testSuite = New-CPSTesterConfig -XMLString $testFileContent

        Format-SimpleLog -MessageType Debug `
            -MessageContent "Test suite: $($testSuite.Title)"

        foreach ($testCase in $testSuite.TestCases) {
            $commandBlock = $testCase.CommandBlock

            Format-SimpleLog -MessageType Debug `
                -MessageContent "Test case: $($testCase.Title)"

            Format-SimpleLog -MessageType Info `
                -MessageContent "Executing command:`n''''''`n$commandBlock`n''''''"

            $expected = $testCase.ExpectedResult
            $result = & $OCIExe run --rm $repoImage sh -c $commandBlock

            if ($result -like "*$($expected)*") {
                Format-SimpleLog -MessageType Success `
                    -MessageContent "Test case $($testCase.Title) passed"

                $passedTests ++
            }
            else {
                Format-SimpleLog -MessageType Error `
                    -MessageContent "Test case $($testCase.Title) failed"

                Format-SimpleLog -MessageType Error `
                    -MessageContent "Expected:`n''''''`n$expected`n''''''"
                Format-SimpleLog -MessageType Error `
                    -MessageContent "but got:`n''''''`n$result`n''''''"

                $failedTests ++
            }
        }
    }

    if ($failedTests -gt 0) {
        Format-SimpleLog -MessageType Error `
            -MessageContent "There were $failedTests failing tests"

        throw "$failedTests tests failed"
    }
    elseif ($passedTests -eq 0) {
        Format-SimpleLog -MessageType Error -MessageContent "No tests were ran"
    }
    else {
        Format-SimpleLog -MessageType Success `
            -MessageContent "All $passedTests tests passed successfully"
    }
}

Export-ModuleMember -Function Invoke-CPSTest
