# -*- make -*-

MAKE            ?= make
SH              ?= sh

CP              := cp
DOTNET          := $(SH) $(SOURCE)/dotnet_wrapper.sh
FIND            := find
OCI-EXE         := docker
PWSH            := $(SH) $(SOURCE)/pwsh_wrapper.sh
PWSHSCRIPT      := $(PWSH) -NonInteractive
PYTHON          := python3
RM              := rm -f -r
