#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Format PowerShell scripts.

.Description
Automatically pick up and format PowerShell scripts from this repository.

.Example
./format_ps.ps1
#>

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

Import-Module PSScriptAnalyzer
Import-Module SimpleLog

Set-Location "$PSScriptRoot/../"

$includedFileExts = @("*.ps1", "*.psm1")
$scriptPaths = Get-ChildItem -File -Include $includedFileExts -Path . -Recurse

foreach ($scriptPath in $scriptPaths) {
    $scriptName = $scriptPath.Name
    $scriptContent = Get-Content -Path $scriptPath -Raw

    $formattedContent = Invoke-Formatter -ScriptDefinition $scriptContent
    $trimmedContent = $formattedContent.Trim()

    Format-SimpleLog -MessageType Debug -MessageContent "Formatting file: $scriptName"

    Set-Content -Path $scriptPath -Value $trimmedContent
}
