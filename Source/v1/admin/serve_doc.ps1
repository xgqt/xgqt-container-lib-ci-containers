#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Serve built documentation.

.Description
Serve documentation of ci-containers.

.Outputs
None.
#>

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

Set-Location -Path "$PSScriptRoot/../ci-containers-documentation/src/"

& "$PSScriptRoot/build_doc.ps1"
& "$PSScriptRoot/mkdocs_wrapper.pl" serve $args
