#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "$(realpath "${script_path}")")"

exec "${script_root}/3rd-party/xo-depot-tools/Source/v1/xo-depot-tools/src/wrap-mkdocs.bash" "${@}"
