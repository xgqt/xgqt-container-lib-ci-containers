pipeline {
    agent any

    tools {
        dotnetsdk 'dotnet-sdk-8.0'
    }

    parameters {
        string(
            defaultValue: 'docker.io',
            description: 'OCI registry',
            name: 'ociRegistry',
            trim: true
        )
    }

    environment {
        OCI_REGISTRY = "${params.ociRegistry}"

        DO_NOT_TRACK = 'true'
        EARTHLY_DISABLE_ANALYTICS = 'true'

        DOTNET_CLI_TELEMETRY_OPTOUT = '1'
        DOTNET_NOLOGO = '1'
        DOTNET_SKIP_FIRST_TIME_EXPERIENCE = '1'
        DOTNET_SYSTEM_GLOBALIZATION_INVARIANT = '1'
        MSBUILDDISABLENODEREUSE = '1'

        POWERSHELL_TELEMETRY_OPTOUT = '1'
        POWERSHELL_UPDATECHECK = '0'

        LogVerbosity = 'minimal'
        UseSharedCompilation = 'false'
    }

    stages {
        stage('checkout') {
            steps {
                checkout scm

                sh 'git submodule update --force --init --recursive'
            }
        }

        stage('build') {
            when {
                anyOf {
                    // Core.
                    changeset 'Jenkinsfile'
                    changeset 'Source/v2/3rd-party/**'
                    changeset 'Source/v2/Makefile'
                    changeset 'Source/v2/VERSION'
                    changeset 'Source/v2/ci-container-*/**'

                    changeset 'Source/v2/ci-posh-build-image/**'
                    changeset 'Source/v2/cicd/ci_build.sh'
                }
            }

            steps {
                sh './Source/v2/cicd/ci_build.sh'
            }
        }

        stage('test') {
            when {
                anyOf {
                    // All from "build".
                    changeset 'Jenkinsfile'
                    changeset 'Source/v2/3rd-party/**'
                    changeset 'Source/v2/Makefile'
                    changeset 'Source/v2/VERSION'
                    changeset 'Source/v2/ci-container-*/**'
                    changeset 'Source/v2/ci-posh-build-image/**'
                    changeset 'Source/v2/cicd/ci_build.sh'

                    changeset 'Source/v2/ci-fs-cpstesterxml/**'
                    changeset 'Source/v2/ci-posh-cpstester/**'
                    changeset 'Source/v2/cicd/ci_test.sh'
                }
            }

            steps {
                sh './Source/v2/cicd/ci_test.sh'
            }
        }

        stage('public') {
            when {
                anyOf {
                    // Core.
                    changeset 'Jenkinsfile'
                    changeset 'Source/v2/3rd-party/**'
                    changeset 'Source/v2/Makefile'
                    changeset 'Source/v2/VERSION'
                    changeset 'Source/v2/ci-container-*/**'

                    changeset 'CHANGELOG.md'
                    changeset 'Earthfile'
                    changeset 'README.md'
                    changeset 'Source/v2/ci-containers-documentation/**'
                    changeset 'Source/v2/ci-posh-cpsdoc/**'
                    changeset 'Source/v2/cicd/ci_public.sh'
                }
            }

            steps {
                sh './Source/v2/cicd/ci_public.sh'
            }
        }
    }
}
