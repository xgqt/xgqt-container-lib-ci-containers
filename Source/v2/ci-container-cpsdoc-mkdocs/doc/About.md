This container image was introduced to support the CI/CD deployment pipeline
of `ci-containers` that uses GitLab pages.

See also:

* [.gitlab-ci.yml](https://gitlab.com/xgqt/xgqt-container-lib-ci-containers/-/blob/master/.gitlab-ci.yml)
* [pages.yml](https://gitlab.com/xgqt/xgqt-container-lib-ci-containers/-/blob/master/Source/v1/ci-containers-gitlab-ci/pages.yml)
