#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

Import-Module SimpleLog

function Invoke-RegenTag {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $DirectoryPath
    )

    $dateFormat = "yyyy.MM.dd.HH.mm"
    $dateTag = Get-Date -Format $dateFormat

    $tagFilePath = Join-Path -Resolve `
        -Path $DirectoryPath -ChildPath "./src/options/TAG"

    if (Test-Path $tagFilePath) {
        $currentTag = (Get-Content $tagFilePath).Trim()

        switch ($currentTag) {
            "current" {
                Format-SimpleLog -MessageType Error `
                    -MessageContent 'Current tag is set to "current", not changing it.'

                exit 0
            }

            default {
                Format-SimpleLog -MessageType Debug `
                    -MessageContent "Old image tag was: $currentTag"
            }
        }
    }

    Format-SimpleLog -MessageType Success -MessageContent "Setting image tag to: $dateTag"

    Set-Content -Path $tagFilePath $dateTag
}

Export-ModuleMember -Function Invoke-RegenTag
