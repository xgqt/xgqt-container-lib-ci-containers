#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

<#
.Synopsis
Redo a container.

.Description
Retag and then build and test a container image subproject.

.Example
./redo_image.ps1 ./ci-container-dotnet-tools
#>

param(
    [string] $BaseDirectory,
    [string] $OCIExe = "docker",

    [switch] $Help
)

$ErrorActionPreference = "Stop"
Set-StrictMode -Version Latest

if ($Help) {
    Get-Help $MyInvocation.MyCommand.Definition -Full

    exit 0
}
elseif ($args) {
    throw "Unknown arguments: $args"
}

$pwshWrapper =  Join-Path -Resolve -Path $PSScriptRoot -ChildPath "../pwsh_wrapper.sh"

& $pwshWrapper "$PSScriptRoot/regen_tag.ps1" `
    -DirectoryPath $BaseDirectory

if ($LASTEXITCODE -ne 0) {
    Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

    throw "regen_tag.ps1 failed"
}

& $pwshWrapper "$PSScriptRoot/build_image.ps1" `
    -BuildDirectoryPath $BaseDirectory -OCIExe $OCIExe

if ($LASTEXITCODE -ne 0) {
    Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

    throw "build_image.ps1 failed"
}

& $pwshWrapper "$PSScriptRoot/test_image.ps1" `
    -BaseDirectory $BaseDirectory -OCIExe $OCIExe

if ($LASTEXITCODE -ne 0) {
    Format-SimpleLog -MessageType Error -MessageContent "Subcommand failed"

    throw "test_image.ps1 failed"
}
