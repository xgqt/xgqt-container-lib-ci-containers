#!/usr/bin/env -S pwsh -NoLogo -NoProfile -NonInteractive

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

$Env:PSModulePath += ":$PSScriptRoot/../../../ci-containers-powershell-util/src:"

Import-Module PSOCIUtil
Import-Module SimpleLog

function Find-OCIBuidlerExe {
    [CmdletBinding()]

    [OutputType([string])]

    param(
        # Allow null.
        [string] $OCIExe
    )

    $ociBuidlerExe = $null
    $supportedOCIExes = @(
        "docker",
        "buildah",
        "podman"
    )

    if ($OCIExe -and `
        (Get-Command $OCIExe `
                -ErrorAction SilentlyContinue -OutVariable ociBuidlerExe)) {
        if (-not $supportedOCIExes.Contains($OCIExe)) {
            Format-SimpleLog -MessageType Error `
                -MessageContent "Using unsupported OCI builder: $OCIExe"
        }
    }
    else {
        foreach ($supportedOCIExe in $supportedOCIExes) {
            if (Get-Command $supportedOCIExe `
                    -ErrorAction SilentlyContinue -OutVariable ociBuidlerExe) {
                break
            }
        }
    }

    return $ociBuidlerExe
}

function Invoke-OCIBuildAndTag {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $ExePath,

        [Parameter(Mandatory)] [array] $BuildArguments,
        [Parameter(Mandatory)] [array] $TagArguments
    )

    $env:BUILDKIT_PROGRESS = "plain"

    Format-SimpleLog -MessageType Debug `
        -MessageContent "OCI builder executable path is: $ExePath"
    Format-SimpleLog -MessageType Success -MessageContent "Executing builder command"
    Format-SimpleLog -MessageType Info -MessageContent "$ExePath $BuildArguments"

    Invoke-OCIExe $ExePath $BuildArguments
    Invoke-OCIExe $ExePath $TagArguments
}

function Invoke-BuildOCIImageCurrent {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $ContainerfilePath,
        [Parameter(Mandatory)] [object] $ArgsJson,

        [Parameter(Mandatory)] [string] $CompleteImageRepo,
        [Parameter(Mandatory)] [string] $CompleteImageTag,

        [string] $OCIExe,
        [Parameter(Mandatory)] [bool] $AutomaticOptions
    )

    $imageNameCurrent = $CompleteImageRepo + ":" + $CompleteImageTag
    $imageNameLatest = $CompleteImageRepo + ":" + "latest"

    Format-SimpleLog -MessageType Success -MessageContent "Image tag configuration"
    Format-SimpleLog -MessageType Info -MessageContent "Image tag is: $imageNameCurrent"

    $ociBuildArguments = @(
        "build",
        "--file", $ContainerfilePath,
        "--tag", $imageNameCurrent
    )
    $ociTagArguments = @(
        "tag",
        $imageNameCurrent,
        $imageNameLatest
    )

    Format-SimpleLog -MessageType Success -MessageContent "Image args configuration"

    foreach ($pair in $ArgsJson.GetEnumerator()) {
        $key = $pair.Key
        $value = $pair.Value

        $ociBuildArguments += "--build-arg"
        $ociBuildArguments += '"' + $pair.Key + '=' + $pair.Value + '"'

        Format-SimpleLog -MessageType Info -MessageContent "$key is: $value"
    }

    $ociBuidlerExe = Find-OCIBuidlerExe $OCIExe

    if (-not $ociBuidlerExe) {
        throw "No OCI executable implementation found."
    }

    if ($AutomaticOptions) {
        switch -wildcard ($ociBuidlerExe.Name) {
            "*buildah*" {
                $ociBuildArguments += "--isolation=chroot"

                break
            }
        }
    }

    $ociBuildArguments += `
        Join-Path -Path $FullBuildDirectoryPath -ChildPath "src"

    Invoke-OCIBuildAndTag `
        -ExePath $ociBuidlerExe.Path `
        -BuildArguments $ociBuildArguments `
        -TagArguments $ociTagArguments

    Format-SimpleLog -MessageType Success `
        -MessageContent "Successfully built image: $imageNameCurrent"
}

function Get-FullBuildDirectoryPath {
    [CmdletBinding()]

    [OutputType([string])]

    param(
        [Parameter(Mandatory)] [string] $BuildDirectoryPath
    )

    $item = Get-Item $BuildDirectoryPath
    $itemFullName = $item.FullName

    return $itemFullName
}

function Invoke-BuildOCIImage {
    [CmdletBinding()]

    param(
        [Parameter(Mandatory)] [string] $OCIRegistry,

        [Parameter(Mandatory)] [string] $BuildDirectoryPath,
        [string] $OCIExe,
        [Parameter(Mandatory)] [bool] $AutomaticOptions
    )

    $FullBuildDirectoryPath = Get-FullBuildDirectoryPath $BuildDirectoryPath

    Set-Location `
        -Path (Join-Path -Path $FullBuildDirectoryPath -ChildPath "src")

    $semanticVersion = Get-ConfigContent "../../VERSION"
    $localTag = Get-ConfigContent "./options/TAG"
    $imageRepo = Get-ConfigContent "./options/REPO"

    $taggingJson = Get-JsonContent "./options/TAGGING.json"

    $containerfilePath = (
        Join-Path `
            -Resolve `
            -Path $FullBuildDirectoryPath `
            -ChildPath "./src/containers/Containerfile"
    )

    $argsJsonMultiple = Get-JsonContent "./options/ARGS.json"

    # Check for dependencies.
    $dependencyFile = Join-Path `
        -Path $FullBuildDirectoryPath `
        -ChildPath "src/options/DEPEND"

    if (Test-Path -Path $dependencyFile) {
        $dependency = Get-ConfigContent -Path $dependencyFile

        Format-SimpleLog -MessageType Debug `
            -MessageContent "Detected $imageRepo dependency: $dependency"

        Invoke-BuildOCIImage `
            -OCIRegistry $OCIRegistry `
            -BuildDirectoryPath "../../$dependency" `
            -OCIExe $OCIExe `
            -AutomaticOptions $AutomaticOptions
    }

    # Perform builds.
    foreach ($argsJsonSingle in $argsJsonMultiple) {
        $completeImageRepo = $OCIRegistry + "/" + $imageRepo
        $completeImageTag = Get-CompleteImageTag `
            -ArgsJson $argsJsonSingle -TaggingJson $taggingJson `
            -SemanticVersion $semanticVersion -LocalTag $localTag

        Invoke-BuildOCIImageCurrent `
            -ContainerfilePath $containerfilePath `
            -ArgsJson $argsJsonSingle `
            -CompleteImageRepo $completeImageRepo `
            -CompleteImageTag $completeImageTag `
            -OCIExe $OCIExe `
            -AutomaticOptions $AutomaticOptions
    }
}

Export-ModuleMember -Function Invoke-BuildOCIImage
