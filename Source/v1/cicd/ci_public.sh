#!/bin/sh

# This Source Code Form is subject to the terms of the Mozilla Public
# License, v. 2.0. If a copy of the MPL was not distributed with this
# file, You can obtain one at http://mozilla.org/MPL/2.0/.

set -e
set -u
set -x
trap 'exit 128' INT

script_path="${0}"
script_root="$(dirname "${script_path}")"

repo_root="$(realpath "${script_root}/../../../")"
cd "${repo_root}"

export DO_NOT_TRACK="true"
export EARTHLY_DISABLE_ANALYTICS="true"

earthly +public-v1

cd ./Build/v1_earthly_BuildDir/artifacts/public/
find . -type f -exec ls -hl {} +
